# Apuntes sobre gestioCI
Gracias por leer este documento, esperamos que ayude a entender las diferentes circustancias que atañen el desarrollo de gestioCI y su puesta en marcha.


## Terminología
Creemos necesario una puesta en común de los siguientes tres conceptos. Una forma de consensuar que cuando hablamos de "A" nos referimos a "la explicación técnica de A".

* **Publicar el código:** este proceso se realiza cuando, más o menos contentas de las funcionalidades especificadas, éstas han sido implementadas y procedemos a poner una etiqueta -de tres dígitos separados por puntos- al código y le otorgamos un nombre clave. Además de esto, enviamos el código al mar abierto del internet para que cualquiera pueda releerlo, reelaborarlo o reproducirlo, según las condiciones que la licencia adjunta implica.

* **Staging:** viene de la expresión "poner en escena"; entendemos este paso como la "puesta en marcha" del código en un entorno real, en un servidor real, accesible desde internet, con datos reales o semi-reales, para analizar posibles puntos ciegos o fallos, para simular la posibilidad de pasar los datos existentes a "producción" y para corroborar que los beneficiarios de nuestro desarrollo están concordes con lo pactado en las especificaciones.

* **Production:** entramos en esta fase únicamente cuando los beneficiarios del desarrollo han utilizado exhaustivamente la herramienta en su fase de "staging", cuando no hay nigún impediente (vease datos incongruentes o duplicados, etc. etc.) y cuando, en definitiva, estamos más que satisfechos de la calidad del desarrollo y pensamos que puede desplegarse en un entorno de complejidades imprevedibles (vease usuarios intentando forzar acceso a datos restringidos, etc. etc.)


## Hechos

* El programa capacitado para cerrar un trimestre *no* se encuentra en _producción_.
* El _código publicado_ (versión 1.2.0) el 31 enero satisfacía las especificaciones vigentes de aquel entonces.
* La primera semana de febrero se añadieron alrededor de 20 funcionalidades a las especificaciones.
* El _código en statging_ actualmente va por la versión 1.10.5, mientras que el _código publicado_ va por la version 1.17.x
* Antes de la permanente _publicaremos el código_ implementando la totalidad de las especificaciones requeridas de más y el mismo día lo pondremos en _staging_.


## Reflexiones personales (efkin)
Creo que este documento es una constatación de que el trabajo de desarrollador incluye también el de la explicación de lo que es un desarrollo. En esto creo que no hemos sido lo suficiente competentes, dado que hemos preferido dedicarnos a escribir código y a mejorar nuestro entendimiento y forma de trabajar en conjunto como desarrolladores. Pero empiezo a notar que cuando acumulas unos conocimientos técnicos hace falta redistribuir los necesarios para poder entenderse con los beneficiarios de dicho conocimiento.
Creo que no hemos sabido dejar claro cuan importante era este "revisar" de la fase de staging por parte de las beneficiarias y cuanto importante es hacer unas buenas especificaciones. Supongo que todas aprendemos nuestra labor prácticandola. Pienso y recomiendo que antes de pasar a _producción_ debe de haber una constatación exhaustiva de las funcionalidades con datos reales.
Creo que muchas veces nos hemos entendido mal, también porque como digo al principio, deberíamos haber sabido cuan de importante es micro-educar sobre lo que hacemos para poder establecer pactos claros.
Lo que me refiero es que si el código no está en _producción_ no quiere decir que la faena "no está hecha". Quiere decir que atraviesa un proceso, en el que no somos los únicos actores. Pasar algo a _producción_ implica asumir la responsabilidad de que algo falle en tierras grises, por eso preferimos controles de calidad exhaustivos en _staging_.
Entiendo que el volumen de datos crece y que hay urgencia en manejarlos por el bien de todas. Pero hacemos lo posible y de una forma interesante.

